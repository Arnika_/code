package pz2;

import java.io.*;
import java.util.*;
import java.util.stream.Collectors;

/** Класс для обработки текста согласно заданию */
public class TextEditor {
    /** Строка, хранящая текст из файла */
    private String string = null;
    /** Шаблон для разбиения строки на предложения */
    private String sentencePattern = "[.!?]+\\s*";
    /** Шаблон для разбиения предложения на слова */
    private String wordPattern = "\\s+[-]\\s+|[\\s,;:()\\[\\]]+";

    /** В конструктор передаём имя файла */
    public TextEditor(String fileName) throws Exception{
        File textFile = new File(fileName);
        StringBuilder builder = new StringBuilder();
        String curLine;

        //Если файл не текстовый, не существует либо это папка, кидаем исключение
        if ((!fileName.matches(".*.txt")) || (!textFile.exists()) || (textFile.isDirectory())) {
            throw new IllegalArgumentException("Wrong filename passed to the constructor");
        } else {
            try (BufferedReader reader = new BufferedReader(new FileReader(textFile))) {
                while ((curLine = reader.readLine()) != null) {
                    builder.append(curLine);
                }
                string = builder.toString();
            } catch (Exception e) {
                throw e;
            }
        }
        System.out.println("Text: " + string);
    }

    /** Собственный класс, сравнивающий предложения */
    private class SentenceComparator implements Comparator<String> {
        @Override
        public int compare(String o1, String o2) {
            int wordsFirst = o1.split(wordPattern).length;
            int wordsSecond = o2.split(wordPattern).length;
            return (wordsFirst < wordsSecond)? -1 : (wordsFirst > wordsSecond)? 1 : 0;
        }
    }

    /** Сортировка пузырьком. Не самый эффективный способ, так как
     * количество слов в предложении вычисляется много раз */
    public void sortAndPrint() {
        List<String> sentences = new ArrayList<>(Arrays.asList(string.split(sentencePattern)));
        int wordsFirst, wordsSecond;
        long curTime = System.currentTimeMillis();
        String temp;

        for (int i = 0; i < sentences.size() - 1; ++i) {
            for (int j = 0; j < sentences.size() - i - 1; ++j) {
                wordsFirst = sentences.get(j).split(wordPattern).length;
                wordsSecond = sentences.get(j + 1).split(wordPattern).length;
                if (wordsFirst > wordsSecond) {
                    temp = sentences.get(j);
                    sentences.set(j, sentences.get(j + 1));
                    sentences.set(j + 1, temp);
                }
            }
        }
        System.out.println("\nFirst method (time = " + (System.currentTimeMillis() - curTime) + "ms)\n");
        print(sentences);
    }

    /** Наиболее быстрый вариант, однако кол-во слов все
     * еще вычисляется при каждом сравнении. Используем метод sort класса ArrayList */
    public void sortAndPrint2() {
        List<String> sentences = new ArrayList<>(Arrays.asList(string.split(sentencePattern)));
        long curTime = System.currentTimeMillis();

        sentences.sort(new SentenceComparator());
        System.out.println("\nSecond method (time = " + (System.currentTimeMillis() - curTime) + "ms)\n");
        print(sentences);
    }

    /** Опять таки не самый бысрый способ. Создаём Map и сортируем пары ключ-значение
     * по значению. EntrySet() преобразует Map в список пар ключ-значение. Sorted() — сортировка */
    public void sortAndPrint3() {
        List<String> sentences = new ArrayList<>(Arrays.asList(string.split(sentencePattern)));
        Map<String, Integer> sentenceWords = new TreeMap<>();
        long curTime = System.currentTimeMillis();

        //Кол-во слов для каждого предложения вычисляется один раз
        sentences.forEach(sentence -> sentenceWords.put(sentence, sentence.split(wordPattern).length));
        sentences = sentenceWords.entrySet()
                .stream()
                .sorted((s1, s2) ->
                        (s1.getValue() > s2.getValue()) ? 1 : (s1.getValue() < s2.getValue())? -1 : 0)
                .map(pair -> pair.getKey())
                .collect(Collectors.toList());
        System.out.println("\nThird method (time = " + (System.currentTimeMillis() - curTime) + "ms)\n");
        print(sentences);
    }

    /** Метод для вывода списка строк */
    private void print(List<String> sentences) {
        for (int i = 0; i < sentences.size(); ++i) {
            System.out.println((i + 1) + ") " + sentences.get(i) + ". Words:" + sentences.get(i).split(wordPattern).length);
        }
    }

    
}
