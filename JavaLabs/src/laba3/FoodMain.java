package laba3;

import java.util.Calendar;

/** Класс для проверки работоспособности Food */
public class FoodMain {
	public static void main(String[] args) {
		Calendar date1 = Calendar.getInstance(), date2 = Calendar.getInstance(),
				date3 = Calendar.getInstance(), date4 = Calendar.getInstance();
		date1.set(2017, Calendar.MARCH, 11, 15, 16, 30);
		date2.set(2017, Calendar.MARCH, 30, 15, 16, 30);
		date3.set(2016, Calendar.NOVEMBER, 22, 10, 15);
		date4.set(2017, Calendar.NOVEMBER, 22, 10, 15);
		Food sausage = new Food("Sausage", "Doctor's", date1, date2, 150, "Zavod", 320);
		sausage.print();
		System.out.println("Shelf-life of sausage: " + sausage.getShelfLife() + " days");
		Food olives = new Food("Olives", "Greece's");
		olives.setCaloricContent(250);
		olives.setPrice(46);
		olives.setExpirationDate(date4);
		olives.setProductionDate(date3);
		System.out.println("Shelf-life of olives: " + olives.getShelfLife() + " days");
	}
}
