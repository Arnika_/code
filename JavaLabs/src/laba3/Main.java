package laba3;

import java.util.Arrays;

/** Класс для проверки работоспособность Cinema и Movie */
public class Main {
	public static void main(String[] args) {
		Cinema cinema = new Cinema();
		cinema.addMovie(new Movie("Lost", 120, "Spielberg",
				6.5f, Arrays.asList("Joley", "Pitt")));
		cinema.addMovie(new Movie("Found", 121, "Worhall",
				3.4f, Arrays.asList("Rosey", "Ashton")));
		cinema.addMovie(new Movie("Shawshank Redemption", 131, "Darabont",
				9.9f, Arrays.asList("Robbins", "Freeman")));
		cinema.sortByRating();
		cinema.setHallsNumber(12);
		cinema.lower(4);
		System.out.println("Average rating of the films in cinema " + cinema.getAverageRating());
	}
}
