package laba3;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.concurrent.TimeUnit;

/** Класс, описывающий продукт питания */
public class Food {
	/** Вид продукта, напр. колбаса */
	private String type;
	/** Название продукта, напр. "Докторская" */
	private String name;
	/** Дата изготовления */
	private Calendar productionDate;
	/** Дата конца срока использования */
	private Calendar expirationDate;
	/** Цена за единицу продукции, напр. банку или 100г. */
	private float price = -1;
	/** Информация о производителе */
	private String manufacturer;
	/** Калорийность на 100г. */
	private float caloricContent = -1;
	/** Формат вывода дат */
	private SimpleDateFormat format = new SimpleDateFormat("dd-MM-yyyy");

	/** Перегруженные конструкторы */
	public Food(String type, String name) {
		this.type = type;
		this.name = name;
	}

	public Food(String type, String name, Calendar productionDate, Calendar expirationDate) {
		this(type, name);
		if (productionDate.after(expirationDate)) {
			throw new IllegalArgumentException("Production date cannot be after expiration");
		}
		this.productionDate = productionDate;
		this.expirationDate = expirationDate;
	}

	public Food(String type, String name, Calendar productionDate, Calendar expirationDate,
	            float price) {
		this(type, name, productionDate, expirationDate);
		if (price < 0) {
			throw new IllegalArgumentException("Price cannot be less than 0");
		}
		this.price = price;
	}

	public Food(String type, String name, Calendar productionDate, Calendar expirationDate,
	            float price, String manufacturer, float caloricContent) {
		this(type, name, productionDate, expirationDate, price);
		if (caloricContent < 0) {
			throw new IllegalArgumentException("Caloric content cannot be less than 0");
		}
		this.manufacturer = manufacturer;
		this.caloricContent = caloricContent;
	}

	/** Метод для вывода информации на экран */
	public void print() {
		StringBuilder str = new StringBuilder();
		str.append("Product: " + type + " " + name + "\n");
		if (productionDate != null) {
			str.append("Production date: " + format.format(productionDate.getTime()) + "\n");
		}
		if (expirationDate != null) {
			str.append("Expiration date: " + format.format(expirationDate.getTime()) + "\n");
		}
		if (price != -1 && caloricContent != -1) {
			str.append("Price - " + price + ", caloric content - " + caloricContent + "\n");
		} else if (price != -1) {
			str.append("Price - " + price + "\n");
		} else if (caloricContent != -1) {
			str.append("Caloric content - " + price + "\n");
		}
		if (manufacturer != null) {
			str.append("Manufacturer: " + manufacturer + "\n");
		}
		System.out.println(str.toString());
	}

	/** Возвращает количество дней хранения как разницу между датой изготовления и датой конца срока */
	public long getShelfLife() {
		if (productionDate == null || expirationDate == null) {
			throw new IllegalStateException("Don't have enough information to calculate shelf-life");
		} else {
			return TimeUnit.DAYS.convert(expirationDate.getTimeInMillis()
					- productionDate.getTimeInMillis(), TimeUnit.MILLISECONDS);
		}
	}

	/* Много геттеров и сеттеров */
	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Calendar getProductionDate() {
		return productionDate;
	}

	public void setProductionDate(Calendar productionDate) {
		if (expirationDate != null && expirationDate.before(productionDate)) {
			throw new IllegalArgumentException("Production date cannot be after expiration date");
		}
		this.productionDate = productionDate;
	}

	public Calendar getExpirationDate() {
		return expirationDate;
	}

	public void setExpirationDate(Calendar expirationDate) {
		if (productionDate != null && productionDate.after(expirationDate)) {
			throw new IllegalArgumentException("Expiration date cannot be before production date");
		}
		this.expirationDate = expirationDate;
	}

	public float getPrice() {
		return price;
	}

	public void setPrice(float price) {
		if (price < 0) {
			throw new IllegalArgumentException("Price cannot be negative!");
		}
		this.price = price;
	}

	public String getManufacturer() {
		return manufacturer;
	}

	public void setManufacturer(String manufacturer) {
		this.manufacturer = manufacturer;
	}

	public float getCaloricContent() {
		return caloricContent;
	}

	public void setCaloricContent(float caloricContent) {
		if (caloricContent < 0) {
			throw new IllegalArgumentException("Caloric content cannot be negative!");
		}
		this.caloricContent = caloricContent;
	}
}
