package laba3;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/** Класс, представляющий фильм */
public class Movie {
	/** Название фильма */
	private String title;
	/** Продолжительность */
	private int duration;
	/** Режиссер */
	private String director;
	/** Рейтинг на IMDb */
	private float IMDbRating = -1;
	/** Список актёров */
	private List<String> actors = new ArrayList<>();

	/* Перегруженные конструкторы */
	public Movie() {
		consoleInput();
	}

	public Movie(String title, int duration) {
		if (duration <= 0) {
			throw new IllegalArgumentException("Illegal duration!");
		}
		this.title = title;
		this.duration = duration;
	}

	public Movie(String title, int duration, String director) {
		this(title, duration);
		this.director = director;
	}

	public Movie(String title, int duration, String director, float rating) {
		this(title, duration, director);
		if (rating < 0 || rating > 10) {
			throw new IllegalArgumentException("Illegal rating!");
		}
		this.IMDbRating = rating;
	}

	public Movie(String title, int duration, String director, float rating, List<String> actors) {
		this(title, duration, director, rating);
		this.actors = new ArrayList<>(actors);
	}

	/** Метод для добавления актёра */
	public void addActor(String actor) {
		this.actors.add(actor);
	}

	/** Консольный ввод информации о фильме */
	public void consoleInput() {
		try (Scanner scanner = new Scanner(System.in)) {
			int dur, actrs;
			float rat;

			System.out.print("Input the title of the movie: ");
			this.title = scanner.nextLine();
			System.out.print("Input the duration of the movie: ");
			while (!scanner.hasNextInt() || ((dur = scanner.nextInt()) <= 0)) {
				scanner.nextLine();
				System.out.print("Wrong duration! Input again integer > 0: ");
			}
			scanner.nextLine();
			this.duration = dur;
			System.out.print("Input the director name: ");
			director = scanner.nextLine();
			System.out.print("Input the IMDb rating: ");
			while (!scanner.hasNextFloat() || (rat = scanner.nextFloat()) < 0 || rat > 10) {
				scanner.nextLine();
				System.out.print("Wrong rating input. Try again ");
			}
			IMDbRating = rat;
			System.out.print("How many actors play in the film? ");
			while (!scanner.hasNextInt() || (actrs = scanner.nextInt()) < 0) {
				scanner.nextLine();
				System.out.print("Wrong number of actors. Try again ");
			}
			scanner.nextLine();
			for (int i = 0; i < actrs; i++) {
				System.out.println("Input name of actor #" + (i + 1));
				actors.add(scanner.nextLine());
			}
		}
	}

	/** Даёт строкове представление фильма */
	@Override
	public String toString() {
		StringBuilder str = new StringBuilder();
		str.append("Title — " + title).append("; duration: " + duration);
		if (director != null) {
			str.append("\nDirector — " + director);
		}
		if (IMDbRating != -1) {
			str.append("\nIMDb rating: " + IMDbRating);
		}
		if (!actors.isEmpty()) {
			str.append("\nActors:\n");
			actors.forEach(actor -> str.append(actor + "\n"));
		}
		return str.toString();
	}

	/* Геттеры и сеттеры для полей. Для названия, продолжительности
		 * и режиссера сеттер не предназначен: поля не могут измениться */
	public String getTitle() {
		return title;
	}

	public int getDuration() {
		return duration;
	}

	public String getDirector() {
		return director;
	}

	public float getIMDbRating() {
		return IMDbRating;
	}

	public void setIMDbRating(float IMDbRating) {
		if (IMDbRating < 0 || IMDbRating > 10) {
			throw new IllegalArgumentException("Illegal rating!");
		}
		this.IMDbRating = IMDbRating;
	}

	public List<String> getActors() {
		return actors;
	}

	public void setActors(List<String> actors) {
		this.actors = new ArrayList<>(actors);
	}
}
