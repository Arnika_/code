package laba3;

import java.util.Arrays;
import java.util.Scanner;
import java.util.stream.Collectors;

/** Класс, представляющий кинотеатр */
public class Cinema {
	/** Название кинотеатра */
	private String name;
	/** Адрес кинотеатра */
	private String address;
	/** Фильмы в прокате */
	private Movie[] movies = new Movie[0];
	/** Количество фильмов */
	private int moviesNumber;
	/** Число залов */
	private int hallsNumber;

	/* Перегруженные конструкторы */
	public Cinema() {
		consoleInput();
	}

	public Cinema(String name, String address) {
		this.name = name;
		this.address = address;
	}

	public Cinema(String name, String address, Movie[] movies) {
		this(name, address);
		this.movies = new Movie[movies.length];
		this.moviesNumber = movies.length;
		System.arraycopy(movies, 0, this.movies, 0, movies.length);
	}

	public Cinema(String name, String address, Movie[] movies, int hallsNumber) {
		this(name, address, movies);
		if (hallsNumber <= 0) {
			throw new IllegalArgumentException("Illegal number of halls");
		}
		this.hallsNumber = hallsNumber;
	}

	/** Добавляем фильм в массив. Если закончилось место,
	 * увеличиваем размер в 2 раза и перезаписываем */
	public void addMovie(Movie movie) {
		if (this.moviesNumber == this.movies.length) {
			Movie[] newMovies = new Movie[this.movies.length * 2 + 1];
			System.arraycopy(this.movies, 0, newMovies, 0, movies.length);
			this.movies = newMovies;
		}
		this.movies[moviesNumber++] = movie;
	}

	/** Консольный ввод информации о кинотеатре */
	public void consoleInput() {
		try (Scanner scanner = new Scanner(System.in)) {
			int numb;

			System.out.print("Input the name of the cinema: ");
			name = scanner.nextLine();
			System.out.println("Input the address of the cinema: ");
			address = scanner.nextLine();
			System.out.println("Input the number of the halls ");
			while (!scanner.hasNextInt() || (numb = scanner.nextInt()) <= 0) {
				scanner.nextLine();
				System.out.print("Wrong number of halls! Input again integer > 0: ");
			}
			hallsNumber = numb;
			System.out.print("How many movies are in the cinema? ");
			while (!scanner.hasNextInt() || (numb = scanner.nextInt()) < 0) {
				scanner.nextLine();
				System.out.print("Wrong number of actors. Try again ");
			}
			movies = new Movie[numb * 2 + 1];
			moviesNumber = 0;
			for (int i = 0; i < numb; i++) {
				System.out.println("Movie 1");
				movies[moviesNumber++] = new Movie();
			}
		}
	}

	/** Вычисляет средний рейтинг фильмов */
	public double getAverageRating() {
		double aver = 0, curRating;
		for (int i = 0; i < moviesNumber; i++) {
			curRating = movies[i].getIMDbRating();
			aver += (curRating == -1)? 0 : curRating;
		}
		return aver / (double)moviesNumber;
	}

	/** Фильмы с рейтингом выше заданного */
	public void higher(double lowerBound) {
		int counter = 1;

		if (lowerBound > 10) {
			throw new IllegalArgumentException("Lower bound cannot be more than 10");
		}
		System.out.println("Films with rating higher than " + lowerBound);
		for (int i = 0; i < moviesNumber; i++) {
			if (movies[i].getIMDbRating() > lowerBound) {
				System.out.println(counter++ + ") " + movies[i]);
			}
		}
	}

	/** Фильмы с рейтингом выше среднего */
	public void higher() {
		int counter = 1;
		double lowerBound = getAverageRating();

		System.out.println("Films with rating higher than average=" + lowerBound);
		for (int i = 0; i < moviesNumber; i++) {
			if (movies[i].getIMDbRating() > lowerBound) {
				System.out.println(counter++ + ") " + movies[i]);
			}
		}
	}

	/** Фильмы с рейтингом ниже заданного */
	public void lower(double upperBound) {
		int counter = 1;

		if (upperBound  < 0) {
			throw new IllegalArgumentException("Upper bound cannot be less than 0");
		}
		System.out.println("Films with rating lower than " + upperBound);
		for (int i = 0; i < moviesNumber; i++) {
			if (movies[i].getIMDbRating() < upperBound) {
				System.out.println(counter++ + ") " + movies[i]);
			}
		}
	}

	/** Фильмы с рейтингом ниже среднего */
	public void lower() {
		int counter = 1;
		double upperBound = getAverageRating();

		System.out.println("Films with rating lower than average=" + upperBound);
		for (int i = 0; i < moviesNumber; i++) {
			if (movies[i].getIMDbRating() < upperBound) {
				System.out.println(counter++ + ") " + movies[i]);
			}
		}
	}

	/** Сортировка фильмов по рейтингу */
	public void sortByRating() {
		if (moviesNumber != 0) {
			movies = Arrays.stream(movies)
					.limit(moviesNumber)
					.sorted((m1, m2) ->
							(m1.getIMDbRating() < m2.getIMDbRating()) ? -1 :
									(m1.getIMDbRating() > m2.getIMDbRating()) ? 1 : 0)
					.collect(Collectors.toList()).toArray(new Movie[0]);
			System.out.println("Movies sorted by rating:");
			for (int i = 0; i < moviesNumber; i++) {
				System.out.println((i + 1) + ") " + movies[i]);
			}
		}
	}

	/** Строковое представление кинотеатра */
	@Override
	public String toString() {
		StringBuilder str = new StringBuilder();
		str.append("Name of the cinema: " + name).append("; address: " + address);
		if (hallsNumber != 0) {
			str.append("\nNumber of halls: " + hallsNumber);
		}
		if (moviesNumber != 0) {
			str.append("\nMovies:\n");
			for (int i = 0; i < moviesNumber; i++) {
				str.append((i + 1) + ") " + movies[i]);
			}
		}
		return str.toString();
	}

	// Геттеры и сеттеры. Для названия, адреса и количества фильмов сеттеров не предназначено.
	public String getName() {
		return name;
	}

	public String getAddress() {
		return address;
	}

	public int getMoviesNumber() {
		return moviesNumber;
	}

	public Movie[] getMovies() {
		return movies;
	}

	public void setMovies(Movie[] movies) {
		this.movies = movies;
	}

	public int getHallsNumber() {
		return hallsNumber;
	}

	public void setHallsNumber(int hallsNumber) {
		if (hallsNumber <= 0) {
			throw new IllegalArgumentException("Halls number cannot be 0 or less");
		}
		this.hallsNumber = hallsNumber;
	}
}
